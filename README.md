# HTTP Server drill

## Instructions to setup the project

1. Clone the project

```sh
git clone https://gitlab.com/shaik-rakhaib/shaik-http-server.git
```

2. cd to the `shaik-http-server` directory

```sh
cd shaik-http-server
```

3. Install dependencies

```sh
npm install
```

4. Start the server

```
node server.js
```
---