const { v4: uuidv4 } = require("uuid");
const express = require("express");
const http = require("http");
const app = express();
const port = 8000;

// Serve static files from the 'public' folder
app.use(express.static("public"));

// Define a route for handling the '/html' endpoint
app.get("/html", (req, res) => {
  res.status(200).send(`<!DOCTYPE html>
      <html>
        <head>
        </head>
        <body>
            <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
            <p> - Martin Fowler</p>
      
        </body>
      </html>`);
});

//will respond with some json content
app.get("/json", (req, res) => {
  res.status(200).json({
    slideshow: {
      author: "Yours Truly",
      date: "date of publication",
      slides: [
        {
          title: "Wake up to WonderWidgets!",
          type: "all",
        },
        {
          items: [
            "Why <em>WonderWidgets</em> are great",
            "Who <em>buys</em> WonderWidgets",
          ],
          title: "Overview",
          type: "all",
        },
      ],
      title: "Sample Slide Show",
    },
  });
});

//will repond with uuid
app.get("/uuid", (req, res) => {
  const generatedUUID = uuidv4();
  res.json({ uuid: generatedUUID });
});

app.get("/status/:statusCode", (req, res) => {
  let statusCode = parseInt(req.params.statusCode);
  res.status(statusCode).send(http.STATUS_CODES[statusCode]);
});

//will respond with a success reponse after delay
app.get("/delay/:delay_in_seconds", (req, res) => {
  const delay_seconds = parseInt(req.params.delay_in_seconds);
  setTimeout(() => {
    res.status(200).send(`success response after ${delay_seconds} seconds`);
  }, delay_seconds * 1000);
});

// Create an HTTP server and listen on port 8000
app.listen(port, () => {
  console.log(`Static server is listening on server http://localhost:${port}`);
});
